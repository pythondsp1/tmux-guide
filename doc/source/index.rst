.. TMUX Guide documentation master file, created by
   sphinx-quickstart on Wed Jan 24 22:56:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TMUX Guide
==========


.. toctree::
    :numbered:
    :includehidden:
    :caption: Contents:

    tmux/tmux
